import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:stereo/locator.dart';
import 'package:stereo/models/playlist.dart';
import 'package:stereo/models/user.dart';

import 'base_model.dart';

class HomeViewModel extends BaseModel {
  var storage = locator<FirebaseStorage>();
  var dataGetter = locator<Firestore>();
  var user = locator<User>();
  var audioPlayer = locator<AudioPlayer>();
  var isPLaying = false;


  void play(File song) {
    isPLaying = true;
    audioPlayer.play(song.path, isLocal: true);
    notifyListeners();
  }

  Future<QuerySnapshot> getSongs() async {
    var x = await dataGetter.collection('songs/' + user.uid).getDocuments();
    return x;
  }

  List<Playlist> getPlaylists() {
    user.playlists.forEach((key, value) {

    });
  }
}
