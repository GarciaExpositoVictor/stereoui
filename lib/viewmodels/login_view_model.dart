import 'package:stereo/constants/route_names.dart';
import 'package:stereo/services/authentication_service.dart';
import 'package:stereo/services/dialog_service.dart';
import 'package:stereo/services/firestore_service.dart';
import 'package:stereo/services/navigation_service.dart';

import '../locator.dart';
import 'base_model.dart';

class LoginViewModel extends BaseModel {
  final AuthenticationService _authenticationService =
      locator<AuthenticationService>();
  final DialogService _dialogService = locator<DialogService>();
  final NavigationService _navigationService = locator<NavigationService>();
  final FirestoreService _firestoreService = locator<FirestoreService>();

  void navigateRegister() {
    _navigationService.navigateTo(SignUpViewRoute, true);
  }

  Future login() async {
    setBusy(true);
    var result = await _authenticationService.googleSignIn();
    setBusy(false);
    if (result != null) {
      var userData = await _firestoreService.getUserData(result.uid);
      registerCurrentUser(userData);
      _navigationService.navigateTo(HomeViewRoute, true);
    } else {
      await _dialogService.showDialog(
        title: "Error",
        description: 'User not found',
      );
    }
  }
}
