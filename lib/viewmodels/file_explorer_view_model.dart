import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stereo/constants/route_names.dart';
import 'package:stereo/services/navigation_service.dart';
import 'package:stereo/viewmodels/base_model.dart';

import '../locator.dart';

class FileExplorerViewModel extends BaseModel {
  final NavigationService _navigationService = locator<NavigationService>();
  final SharedPreferences sharedPreferences = locator<SharedPreferences>();

  void getFilePath() async {
    var filePath = await FilePicker.getFilePath();
    if (filePath != null) {
      /** 
       * Pls don't kill me, had to do this in order to get the actual directory
       */
      var parentDirectory = Directory(filePath);
      var path = parentDirectory.parent.path + '/';
      var fileDirectory = Directory(path);
      /**
       * Dunno why flutter cannot get all files from the directory of the same file
       * Instead it gets all the files from the parent directory...
       */
      sharedPreferences.setString(SHSongDirectoryKey, path);
      var files = fileDirectory.listSync().where((element) => element is File).toList();
      _navigationService.navigateTo(HomeViewRoute, true, arguments: files);
    }
  }
}
