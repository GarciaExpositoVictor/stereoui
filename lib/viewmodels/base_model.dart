import 'package:flutter/material.dart';
import 'package:stereo/models/user.dart';

import '../locator.dart';

class BaseModel extends ChangeNotifier {
  User get currentUser {
    return locator<User>();
  }

  bool _busy = false;
  bool get busy => _busy;

  void setBusy(bool value) {
    _busy = value;
    notifyListeners();
  }
}
