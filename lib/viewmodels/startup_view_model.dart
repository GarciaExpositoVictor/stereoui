import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stereo/constants/route_names.dart';
import 'package:stereo/models/user.dart';
import 'package:stereo/services/authentication_service.dart';
import 'package:stereo/services/firestore_service.dart';
import 'package:stereo/services/navigation_service.dart';

import '../locator.dart';
import 'base_model.dart';

class StartUpViewModel extends BaseModel {
  final AuthenticationService _authenticationService =
      locator<AuthenticationService>();
  final NavigationService _navigationService = locator<NavigationService>();
  final FirestoreService _firestoreService = locator<FirestoreService>();

  Future handleStartUpLogic() async {
    var permission = await Permission.storage.request();
    if (permission.isGranted) {
      var hasLoggedInUser = await _authenticationService.getUser;
      if (hasLoggedInUser != null) {
        var userData = await _firestoreService.getUserData(hasLoggedInUser.uid);
        if (userData == null) {
          var user = User(
              playlists: Map<String, dynamic>(),
              profilePicture: hasLoggedInUser.photoUrl,
              tags: List<String>(),
              uid: hasLoggedInUser.uid,
              username: hasLoggedInUser.displayName);
          _firestoreService.createUser(user);
          registerCurrentUser(user.toJson());
        } else {
          registerCurrentUser(userData);
        }
        var shInstance = await SharedPreferences.getInstance();
        registerSharedPreferences(shInstance);
        var directory = shInstance.getString(SHSongDirectoryKey);
        if ( directory == '' || directory == null) {
          _navigationService.navigateTo(FileExplorerRoute, true);
        } else {
          print(shInstance.getString(SHSongDirectoryKey));
          _navigationService.navigateTo(HomeViewRoute, true);
        }
      } else {
        _navigationService.navigateTo(LoginViewRoute, true);
      }
    } else {
      SystemNavigator.pop();
    }
  }
}
