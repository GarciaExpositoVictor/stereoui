import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:stereo/locator.dart';
import 'package:stereo/router.dart';
import 'package:stereo/services/dialog_service.dart';
import 'package:stereo/services/navigation_service.dart';
import 'package:stereo/ui/views/startup_view.dart';

import 'managers/dialog_manager.dart';

void main() {
  setupLocator();
  runApp(MyApp());
  SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
} 

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stereo',
      builder: (context, child) => Navigator(
        key: locator<DialogService>().dialogNavigationKey,
        onGenerateRoute: (settings) => MaterialPageRoute(builder: (context) => DialogManager(child: child)),
      ),
      navigatorKey: locator<NavigationService>().navigationKey,
      theme: ThemeData(
        fontFamily: 'Baloo',
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: StartUpView(),
      onGenerateRoute: generateRoute,
    );
  }
}
