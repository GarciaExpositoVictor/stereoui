import 'dart:io';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:stacked/stacked.dart';
import 'package:stereo/ui/widgets/song_list.dart';
import 'package:stereo/viewmodels/home_view_model.dart';

class HomeView extends StatelessWidget {
  final List<FileSystemEntity> files;

  const HomeView({Key key, this.files}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeViewModel>.reactive(
      viewModelBuilder: () => HomeViewModel(),
      onModelReady: (model) => {
        //model.getSongs(),
      },
      builder: (context, model, child) => Scaffold(
        backgroundColor: Color(0xFFc5d9e0),
        appBar: AppBar(
          backgroundColor: Color(0xFFc5d9e0),
          elevation: 0.0,
          title: Row(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 10.0),
                child: Text(
                  'stere',
                  style: TextStyle(
                    color: Color.fromRGBO(50, 67, 74, 10.0),
                    fontFamily: 'Baloo',
                    fontWeight: FontWeight.bold,
                    height: 18.0,
                    fontSize: 35.0,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 20.0),
                child: Icon(
                  FontAwesomeIcons.compactDisc,
                  color: Color.fromRGBO(50, 67, 74, 10.0),
                  size: 20,
                ),
              ),
            ],
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
          ),
        ),
        body: Stack(
          fit: StackFit.loose,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              child: SingleChildScrollView(
                dragStartBehavior: DragStartBehavior.start,
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                controller: ScrollController(
                  initialScrollOffset: 0,
                  keepScrollOffset: true,
                ),
                child: Row(
                  children: <Widget>[
                    ListView.builder(
                      shrinkWrap: true,
                      physics: BouncingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      itemCount: 2,
                      itemBuilder: (BuildContext context, int index) {
                        return SongList(model: model,files: files);
                      },
                    ),
                  ],
                ),
              ),
            ),
            //MusicCollections(model: model),
          ],
        ),
      ),
    );
  }
}
