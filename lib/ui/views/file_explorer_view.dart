import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:stacked/stacked.dart';
import 'package:stereo/viewmodels/file_explorer_view_model.dart';

class FileExplorerView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<FileExplorerViewModel>.reactive(
      viewModelBuilder: () => FileExplorerViewModel(),
      builder: (context, model, child) => Scaffold(
        backgroundColor: Color(0xFFf5eae0),
        body: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  child: Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width / 4,
                          top: 100,
                        ),
                        child: Text(
                          'stere',
                          style: TextStyle(
                            color: Color.fromRGBO(50, 67, 74, 10.0),
                            fontFamily: 'Baloo',
                            fontWeight: FontWeight.bold,
                            fontSize: 70,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 73),
                        child: Icon(
                          FontAwesomeIcons.compactDisc,
                          color: Color.fromRGBO(50, 67, 74, 10.0),
                          size: 38,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/images/logovinyl.png'),
                      fit: BoxFit.scaleDown,
                    ),
                  ),
                  padding: EdgeInsets.only(left: 10.0),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height / 1.5,
                    left: MediaQuery.of(context).size.width / 25,
                  ),
                  height: MediaQuery.of(context).size.height / 3,
                  child: FlatButton(
                    textColor: Color(0xFFc5d9e0),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(14),
                    ),
                    onPressed: () async => model.getFilePath(),
                    child: Text(
                      'Tap to choose your music folder',
                      style: TextStyle(
                        fontSize: MediaQuery.of(context).size.width / 17,
                        color: Color.fromRGBO(50, 67, 74, 10.0),
                        fontFamily: 'Baloo',
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
