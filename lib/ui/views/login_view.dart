import 'package:apple_sign_in/apple_sign_in_button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:stacked/stacked.dart';
import 'package:stereo/services/authentication_service.dart';
import 'package:stereo/viewmodels/login_view_model.dart';

import '../../locator.dart';

class LoginView extends StatelessWidget {
  final AuthenticationService auth = locator<AuthenticationService>();

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<LoginViewModel>.reactive(
      builder: (context, model, child) => Scaffold(
        body: Container(
          padding: EdgeInsets.all(30),
          decoration: BoxDecoration(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                width: 350,
                height: 300,
                child: Image.asset('assets/images/logovinyl.png'),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'stere',
                    style: TextStyle(
                      color: Color.fromRGBO(50, 67, 74, 10.0),
                      fontFamily: 'Baloo',
                      fontWeight: FontWeight.bold,
                      fontSize: 35.0,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 12.0),
                    child: Icon(
                      FontAwesomeIcons.recordVinyl,
                      color: Color.fromRGBO(50, 67, 74, 10.0),
                      size: 25.0,
                    ),
                  )
                ],
              ),
              Text('Where music happens'),
              LoginButton(
                text: 'LOGIN WITH GOOGLE',
                icon: Icons.verified_user,
                color: Color(0xFFf75d30),
                loginMethod: auth.googleSignIn,
              ),
              FutureBuilder<Object>(
                future: auth.appleSignInAvailable,
                builder: (context, snapshot) {
                  if (snapshot.data == true) {
                    return AppleSignInButton(
                      onPressed: () async {
                        FirebaseUser user = await auth.appleSignIn();
                        if (user != null) {
                          Navigator.pushReplacementNamed(context, '/topics');
                        }
                      },
                    );
                  } else {
                    return Container();
                  }
                },
              ),
            ],
          ),
        ),
      ),
      viewModelBuilder: () => LoginViewModel(),
    );
  }
}

class LoginButton extends StatelessWidget {
  final Color color;
  final IconData icon;
  final String text;
  final Function loginMethod;

  const LoginButton(
      {Key key, this.text, this.icon, this.color, this.loginMethod})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      child: FlatButton.icon(
        padding: EdgeInsets.all(30),
        icon: Icon(icon, color: Colors.white),
        color: color,
        onPressed: () async {
          var user = await loginMethod();
          if (user != null) {
            Navigator.pushReplacementNamed(context, '/topics');
          }
        },
        label: Expanded(
          child: Text('$text', textAlign: TextAlign.center),
        ),
      ),
    );
  }
}
