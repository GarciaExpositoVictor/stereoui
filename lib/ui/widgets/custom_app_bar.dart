import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Color(0xFFc5d9e0),
      elevation: 0.0,
      title: Row(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 10.0),
            child: Text(
              'stere',
              style: TextStyle(
                  color: Color.fromRGBO(50, 67, 74, 10.0),
                  fontFamily: 'Baloo',
                  fontWeight: FontWeight.bold,
                  height: 18.0,
                  fontSize: 35.0),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 20.0),
            child: Icon(
              Icons.pause_circle_filled,
              color: Color.fromRGBO(50, 67, 74, 10.0),
              size: 25.0,
            ),
          ),
        ],
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
      ),
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(20.0);
}
