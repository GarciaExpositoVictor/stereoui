import 'dart:io';

import 'package:flutter/material.dart';
import 'package:stereo/ui/widgets/song_list_item.dart';
import 'package:stereo/viewmodels/home_view_model.dart';

class SongList extends StatelessWidget {
  final HomeViewModel model;
  final List<FileSystemEntity> files;

  const SongList({this.files, this.model});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: MediaQuery.of(context).size.width / 1.12,
      color: Color(0xFFc5d9e0),
      padding: EdgeInsets.only(right: 20),
      margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 12),
      child: ListView.builder(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(
          vertical: MediaQuery.of(context).size.height * 0.33,
        ),
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.vertical,
        itemCount: files != null? files.length : 0,
        itemBuilder: (BuildContext context, int index) {
          return SongListItem(
            model: model,
            song: files[index],
          );
        },
      ),
    );
  }
}
