import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:stereo/models/playlist.dart';

class MusicCollectionItem extends StatelessWidget {
  final List<Playlist> list;

  const MusicCollectionItem({this.list});

  @override
  Widget build(BuildContext context) {
    list.forEach(
      (playlist) {
        return Container(
          padding: EdgeInsets.only(left: 15.0, top: 8),
          width: 100,
          margin: EdgeInsets.only(right: 20.0),
          decoration: BoxDecoration(
            color: Color(0xFFd4e6ea),
            borderRadius: BorderRadius.all(
              Radius.elliptical(30, 30),
            ),
          ),
          child: Column(
            children: <Widget>[
              ListTile(
                title: Text(
                  (list.indexOf(playlist) + 1).toString(),
                  style: TextStyle(fontSize: 30.0),
                ),
                subtitle: Text(
                  playlist.playlistName,
                  style: TextStyle(fontSize: 16.0, letterSpacing: 0.5),
                ),
              ),
              ButtonBar(
                buttonPadding:
                    EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                alignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(
                    FontAwesomeIcons.recordVinyl,
                    color: Colors.black54,
                    size: 10.0,
                  ),
                  Text(
                    '192 songs',
                    style: TextStyle(color: Colors.black54, fontSize: 10.0),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
