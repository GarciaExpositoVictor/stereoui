import 'package:flutter/material.dart';
import 'package:stereo/viewmodels/home_view_model.dart';

import 'music_collection_item.dart';

class MusicCollections extends StatelessWidget {
  final HomeViewModel model;

  const MusicCollections({this.model});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 205,
      width: 100,
      margin: EdgeInsets.only(top: 20.0),
      child: ListView.builder(
        padding: EdgeInsets.symmetric(horizontal: 30.0),
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        itemCount: 12,
        itemBuilder: (BuildContext context, int index) {
          return MusicCollectionItem(
            list: model.getPlaylists(),
          );
        },
      ),
    );
  }
}
