import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:stereo/viewmodels/home_view_model.dart';
import 'package:path/path.dart';

class SongListItem extends StatelessWidget {
  final HomeViewModel model;
  final File song;

  const SongListItem({this.song, this.model});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 15.0, top: 8),
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(bottom: 10.0),
      height: 70,
      decoration: BoxDecoration(
        color: Color(0xFFd4e6ea),
        borderRadius: BorderRadius.all(
          Radius.elliptical(30, 30),
        ),
      ),
      child: Row(
        children: <Widget>[
          //Album cover
          Column(
            children: <Widget>[
              IconButton(
                alignment: Alignment.center,
                icon: Icon(
                  FontAwesomeIcons.compactDisc,
                  size: 35,
                ),
                onPressed: () {},
              ),
            ],
          ),
          //Song title
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width / 15),
                child: Text(basename(song.path).split('.')[0]),
              ),
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width / 15),
                child: Text(
                  'Default artist',
                ),
              )
            ],
          ),
          //Play button
          Column(
            children: <Widget>[
              Container(
                alignment: Alignment.centerRight,
                padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width / 100,
                ),
                child: RawMaterialButton(
                  onPressed: () => model.play(song),
                  elevation: 0.0,
                  child: Icon(
                    model.isPLaying
                        ? Icons.play_circle_outline
                        : Icons.pause_circle_outline,
                    color: Color(0xFFf75d30),
                    size: MediaQuery.of(context).size.width / 8,
                  ),
                  shape: CircleBorder(),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
