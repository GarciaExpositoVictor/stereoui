import 'package:audioplayers/audioplayers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stereo/services/authentication_service.dart';
import 'package:stereo/services/cloud_storage_service.dart';
import 'package:stereo/services/dialog_service.dart';
import 'package:stereo/services/firestore_service.dart';
import 'package:stereo/services/navigation_service.dart';

import 'models/user.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => AuthenticationService());
  locator.registerLazySingleton(() => FirestoreService()); 
  locator.registerLazySingleton(() => DialogService());
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => FirebaseStorage());
  locator.registerLazySingleton(() => Firestore());
  locator.registerLazySingleton(() => CloudStorageService());
  locator.registerLazySingleton(() => AudioPlayer(mode: PlayerMode.MEDIA_PLAYER));
}

void registerCurrentUser(Map<String, dynamic> data) {
  locator.registerLazySingleton(() => User.fromData(data));
}

void registerSharedPreferences(SharedPreferences instance) {
  locator.registerLazySingleton(() => instance);
}