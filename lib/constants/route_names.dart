const String LoginViewRoute = "LoginView";
const String SignUpViewRoute = "SignUp";
const String FileExplorerRoute = "FileExplorerView";
const String HomeViewRoute = "HomeView";
/**
 * SH stands for shared preferences
 */
const String SHSongDirectoryKey = 'songsDirectory';
