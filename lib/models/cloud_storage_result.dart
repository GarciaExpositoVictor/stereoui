class CloudStorageResult {
  final String songUrl;
  final String songName;

  CloudStorageResult({this.songUrl, this.songName});

}