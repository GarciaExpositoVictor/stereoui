import 'package:flutter/material.dart';

class DialogRequest {
  final String title;
  final String description;
  final String buttonTitle;
  final String cancelTitle;
  final bool hasTextArea;

  DialogRequest(
      {@required this.title,
      @required this.description,
      @required this.buttonTitle,
      this.hasTextArea = false,
      this.cancelTitle});
}
