class Playlist {
  final String playlistName;
  final List<String> playlistSongs;

  Playlist(this.playlistName, this.playlistSongs);

  Playlist.fromData(Map<String, dynamic> data)
      : playlistName = data['playlistName'],
        playlistSongs = data['playlistSongs'];

  Map<String, dynamic> toJson() {
    return {
      'playlistName': playlistName,
      'playlistSongs': playlistSongs,
    };
  }
}