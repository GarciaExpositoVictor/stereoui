class DialogResponse {
  final String fieldOne;
  final String fieldTwo;
  final String textField;
  final bool confirmed;

  DialogResponse({
    this.fieldOne,
    this.fieldTwo,
    this.confirmed,
    this.textField, 
  });
}