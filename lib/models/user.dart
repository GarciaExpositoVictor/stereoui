class User {
  final String username;
  final String uid;
  String profilePicture;
  List<dynamic> tags;
  Map<String, dynamic> playlists;
  Map<String, dynamic> songs;

  User(
      {this.playlists,
      this.profilePicture,
      this.tags,
      this.uid,
      this.username});

  User.fromData(Map<String, dynamic> data)
      : username = data['username'],
        uid = data['uid'],
        profilePicture = data['profilePicture'],
        tags = data['tags'],
        playlists = data['playlists'];

  Map<String, dynamic> toJson() {
    return {
      'username': username,
      'uid': uid,
      'profilePicture': profilePicture,
      'tags': tags,
      'playlists': playlists,
    };
  }
}

class Song {
  final String _id;
  final String album;
  final String author;
  final String description;
  final String duration;
  final String name;
  final String url;

  Song(this._id, this.album, this.author, this.description, this.duration,
      this.name, this.url);

  Song.fromData(Map<String, dynamic> data)
      : _id = data['_id'],
        album = data['album'],
        author = data['author'],
        description = data['description'],
        duration = data['duration'],
        name = data['name'],
        url = data['url'];

  Map<String, dynamic> toJson() {
    return {
      '_id': _id,
      'album': album,
      'author': author,
      'description': description,
      'duration': duration,
      'name': name,
      'url': url,
    };
  }
}

class Tag {
  final String name;

  Tag({this.name});

  Tag.fromData(Map<String, dynamic> data) : name = data['name'];

  Map<String, dynamic> toJson() {
    return {'name': name};
  }
}
