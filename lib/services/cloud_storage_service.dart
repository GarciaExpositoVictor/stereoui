import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:stereo/locator.dart';
import 'package:stereo/models/cloud_storage_result.dart';
import 'package:stereo/models/user.dart';

class CloudStorageService {
  var user = locator<User>();
  var songStorage = locator<FirebaseStorage>().ref().child('songs/');
  Future<CloudStorageResult> uploadMusic(
      {@required File song, @required String title}) async {
    var songRoute = songStorage.child(user.uid + '/' + title);
    var uploadTask = songRoute.putFile(song);
    Future.any([uploadTask.onComplete]).then(
      (value) {
        if (uploadTask.isSuccessful) {
          return CloudStorageResult(
              songName: title, songUrl: value.ref.getDownloadURL().toString());
        } else {
          return null;
        }
      },
      onError: () {
        return null;
      },
    );
    return null;
  }

  Future deleteSong(String song) {
    var songRoute = locator<FirebaseStorage>().ref().child(song);

    Future.any([songRoute.delete()]).then(
      (value) {
        return true;
      },
      onError: () {
        return false;
      },
    );
  }
}
