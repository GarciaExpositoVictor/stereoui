import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:stereo/models/user.dart';

class FirestoreService {
  final CollectionReference _userCollectionReference =
      Firestore.instance.collection('users');

  Future<Map<String, dynamic>> getUserData(String uid) async {
    var userData = await _userCollectionReference.document(uid).get();
    if (userData != null) {
      return userData.data;
    } else {
     return null; 
    }
  }

  Future createUser(User user) async {
    try {
      await _userCollectionReference.document(user.uid).setData(user.toJson());
    } catch (e) {
      if (e is PlatformException) {
        return e.message;
      }

      return e.toString();
    }
  }

  Future updateUser(
      {final String username,
      final String uid,
      String profilePicture,
      Map<String,dynamic> tags,
      Map<String,dynamic> playlists}) async {
    await _userCollectionReference.document(uid).updateData({
      'username': username,
      'uid': uid,
      'profilePicture': profilePicture,
      'tags': tags,
      'playlists': playlists,
    });
  }
}
