import 'package:flutter/material.dart';
import 'package:stereo/ui/views/file_explorer_view.dart';
import 'package:stereo/ui/views/home_view.dart';
import 'package:stereo/ui/views/login_view.dart';

import 'constants/route_names.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case LoginViewRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: LoginView(),
      );
    case HomeViewRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: HomeView(files: settings.arguments),
      );
    case FileExplorerRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: FileExplorerView(),
      );
    default:
      return MaterialPageRoute(
        builder: (_) => Scaffold(
          body: Center(child: Text('No route defined for ${settings.name}')),
        ),
      );
  }
}

PageRoute _getPageRoute({String routeName, Widget viewToShow}) {
  return MaterialPageRoute(
      settings: RouteSettings(
        name: routeName,
      ),
      builder: (_) => viewToShow);
}
